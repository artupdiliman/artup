﻿namespace Mapbox.Unity.Ar
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using GameSparks.Core;
	using UnityEngine.UI;

	public class MessageService : MonoBehaviour {

		/// <summary>
		/// This class handles communication with gamesparks for
		/// removing, loading, and writing new messages. New Message
		/// objects are instantiated here.
		/// </summary>
		private static MessageService _instance;
		public static MessageService Instance { get { return _instance; } } 

		public GameObject ArtDatabase;
		public Transform mapRootTransform;

		public GameObject messagePrefabAR;
		public GameObject messagePrefabARLove;
		public GameObject messagePrefabARAngery;
		public GameObject messagePrefabARHappy;
		public GameObject messagePrefabARSad;

		public Text nearbySculpture;
		public bool commentedInCurrentARSesh = false;
		public int latestID;
		public double latestLat;
		public double latestLong;
		public string latestText;
		public string latestReacc;

		void Awake(){
			_instance = this;
		}

		public void RemoveAllMessages(){
			new GameSparks.Api.Requests.LogEventRequest ()
				.SetEventKey ("REMOVE_MESSAGES")
				.Send ((response) => {
				if (!response.HasErrors) {
					Debug.Log ("Message Saved To GameSparks...");
				} else {
					Debug.Log ("Error Saving Message Data...");
				}
			});
		}

		public void LoadAllMessages(){

			List<GameObject> messageObjectList = new List<GameObject> ();
			
			new GameSparks.Api.Requests.LogEventRequest().SetEventKey("LOAD_MESSAGE").Send((response) => {
				if (!response.HasErrors) {
					Debug.Log("Received Player Data From GameSparks...");
					List<GSData> locations = response.ScriptData.GetGSDataList ("all_Messages");
					for (var e = locations.GetEnumerator (); e.MoveNext ();) {

						string messageReacc = e.Current.GetString ("messReacc");
						GameObject MessageBubble;

						// do not display comments
						if ((messageReacc != "love") && (messageReacc != "angery") && (messageReacc != "happy") && (messageReacc != "sad")) {
							MessageBubble = (GameObject)Instantiate (messagePrefabAR,mapRootTransform);
							Message message = MessageBubble.GetComponent<Message>();
							message.latitude = double.Parse(e.Current.GetString ("messLat"));
							message.longitude = double.Parse(e.Current.GetString ("messLon"));
							message.text = e.Current.GetString ("messText");
							message.reacc = e.Current.GetString ("messReacc");
							messageObjectList.Add(MessageBubble);
						}
					}
				} else {
					Debug.Log("Error Loading Message Data...");
				}
			});
			if (commentedInCurrentARSesh == true) {
				// add user's local comment
				GameObject MessageBubbleLocal;

				if (latestReacc == "love") {
					MessageBubbleLocal = (GameObject)Instantiate (messagePrefabARLove,mapRootTransform);
				}
				else if (latestReacc == "angery") {
					MessageBubbleLocal = (GameObject)Instantiate (messagePrefabARAngery,mapRootTransform);
				}
				else if (latestReacc == "happy") {
					MessageBubbleLocal = (GameObject)Instantiate (messagePrefabARHappy,mapRootTransform);
				}
				else if (latestReacc == "sad") {
					MessageBubbleLocal = (GameObject)Instantiate (messagePrefabARSad,mapRootTransform);
				}
				else {
					MessageBubbleLocal = (GameObject)Instantiate (messagePrefabAR,mapRootTransform);
				}

				Message messageLocal = MessageBubbleLocal.GetComponent<Message>();

				messageLocal.latitude = latestLat;
				messageLocal.longitude = latestLong;
				messageLocal.text = latestText;
				messageLocal.reacc = latestReacc;

				messageObjectList.Add(MessageBubbleLocal);
			}
			//pass list of objects to ARmessage provider so they can be placed
			ARMessageProvider.Instance.LoadARMessages (messageObjectList);
		}

		public void SaveMessage(int nearbyID, double lat, double lon, string text, string reacc){
			if (nearbyID != -1) {
				new GameSparks.Api.Requests.LogEventRequest ()

					.SetEventKey ("SAVE_GEO_MESSAGE")
					.SetEventAttribute ("ARTID", nearbyID)
					.SetEventAttribute ("LAT", lat.ToString())
					.SetEventAttribute ("LON", lon.ToString())
					.SetEventAttribute ("TEXT", text)
					.SetEventAttribute ("REACC", reacc)
					.Send ((response) => {

						if (!response.HasErrors) {
							Debug.Log ("Message Saved To GameSparks...");
						} else {
							Debug.Log ("Error Saving Message Data...");
						}
					});

				commentedInCurrentARSesh = true;
				latestID = nearbyID;
				latestLat = lat;
				latestLong = lon;
				latestText = text;
				latestReacc = reacc;

				ArtDatabase.GetComponent<Mapbox.Unity.Utilities.ArtDatabase> ().UpdateComments();
			}
		}
	}
}
