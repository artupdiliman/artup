﻿namespace Mapbox.Unity.Ar
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.EventSystems;

	public class UIBehavior : MonoBehaviour {
		/// <summary>
		/// This class houses functions that get called when buttons are pressed 
		/// in the UI.
		/// </summary>
		private static UIBehavior _instance;
		public static UIBehavior Instance { get { return _instance; } } 

		public GameObject MainNav;
		public GameObject HomeScreen;
		public GameObject MessageScreen;
		public GameObject LoveMessageScreen;
		public GameObject AngeryMessageScreen;
		public GameObject HappyMessageScreen;
		public GameObject SadMessageScreen;
		public InputField messageText;
		public InputField loveMessageText;
		public InputField angeryMessageText;
		public InputField happyMessageText;
		public InputField sadMessageText;

		public Text nearbySculpture;

		void Awake(){
			_instance = this;
			HomeScreen.SetActive (false);
			MessageScreen.SetActive (false);
			LoveMessageScreen.SetActive (false);
			AngeryMessageScreen.SetActive (false);
			HappyMessageScreen.SetActive (false);
			SadMessageScreen.SetActive (false);
		}

		public void ShowUI(){
			HomeScreen.SetActive (true);
		}

		public void RemoveButtonDown(){
			HomeScreen.SetActive (false);
			MessageService.Instance.RemoveAllMessages ();
			ARMessageProvider.Instance.RemoveCurrentMessages ();
			StartCoroutine (DelayRemoveRoutine ());
		}

		IEnumerator DelayRemoveRoutine(){
			yield return new WaitForSeconds (2f);
			HomeScreen.SetActive (true);
		}

		/*
		public void MessageButtonDown(string emoticon){
			HomeScreen.SetActive (false);
			MessageScreen.SetActive (true);
		}
		*/

		/*
		public void SubmitButtonDown(){
			double lat = ARMessageProvider.Instance.deviceLocation._currentLocation.LatitudeLongitude.x;
			double lon = ARMessageProvider.Instance.deviceLocation._currentLocation.LatitudeLongitude.y;

			MessageService.Instance.SaveMessage (lat, lon, messageText.text);

			messageText.text = "";
			HomeScreen.SetActive (true);
			MessageScreen.SetActive (false);
			StartCoroutine (DelayLoadMessagesRoutine ());
		}
		*/

		public void MessageButtonDownLove(string emoticon){
			MainNav.SetActive (false);
			HomeScreen.SetActive (false);
			LoveMessageScreen.SetActive (true);
		}

		public void SubmitButtonDownLove(){
			double lat = ARMessageProvider.Instance.deviceLocation._currentLocation.LatitudeLongitude.x;
			double lon = ARMessageProvider.Instance.deviceLocation._currentLocation.LatitudeLongitude.y;

			MessageService.Instance.SaveMessage (int.Parse(nearbySculpture.text), lat, lon, loveMessageText.text, "love");

			loveMessageText.text = "";
			MainNav.SetActive (true);
			HomeScreen.SetActive (true);
			LoveMessageScreen.SetActive (false);
			StartCoroutine (DelayLoadMessagesRoutine ());
		}

		public void MessageButtonDownAngery(string emoticon){
			MainNav.SetActive (false);
			HomeScreen.SetActive (false);
			AngeryMessageScreen.SetActive (true);
		}

		public void SubmitButtonDownAngery(){
			double lat = ARMessageProvider.Instance.deviceLocation._currentLocation.LatitudeLongitude.x;
			double lon = ARMessageProvider.Instance.deviceLocation._currentLocation.LatitudeLongitude.y;

			MessageService.Instance.SaveMessage (int.Parse(nearbySculpture.text), lat, lon, angeryMessageText.text, "angery");

			angeryMessageText.text = "";
			MainNav.SetActive (true);
			HomeScreen.SetActive (true);
			AngeryMessageScreen.SetActive (false);
			StartCoroutine (DelayLoadMessagesRoutine ());
		}

		public void MessageButtonDownHappy(string emoticon){
			MainNav.SetActive (false);
			HomeScreen.SetActive (false);
			HappyMessageScreen.SetActive (true);
		}

		public void SubmitButtonDownHappy(){
			double lat = ARMessageProvider.Instance.deviceLocation._currentLocation.LatitudeLongitude.x;
			double lon = ARMessageProvider.Instance.deviceLocation._currentLocation.LatitudeLongitude.y;

			MessageService.Instance.SaveMessage (int.Parse(nearbySculpture.text), lat, lon, happyMessageText.text, "happy");

			happyMessageText.text = "";
			MainNav.SetActive (true);
			HomeScreen.SetActive (true);
			HappyMessageScreen.SetActive (false);
			StartCoroutine (DelayLoadMessagesRoutine ());
		}

		public void MessageButtonDownSad(string emoticon){
			MainNav.SetActive (false);
			HomeScreen.SetActive (false);
			SadMessageScreen.SetActive (true);
		}

		public void SubmitButtonDownSad(){
			double lat = ARMessageProvider.Instance.deviceLocation._currentLocation.LatitudeLongitude.x;
			double lon = ARMessageProvider.Instance.deviceLocation._currentLocation.LatitudeLongitude.y;

			MessageService.Instance.SaveMessage (int.Parse(nearbySculpture.text), lat, lon, sadMessageText.text, "sad");

			sadMessageText.text = "";
			MainNav.SetActive (true);
			HomeScreen.SetActive (true);
			SadMessageScreen.SetActive (false);
			StartCoroutine (DelayLoadMessagesRoutine ());
		}


		IEnumerator DelayLoadMessagesRoutine(){
			yield return new WaitForSeconds (1f);
			MessageService.Instance.LoadAllMessages ();
		}
	}
}
