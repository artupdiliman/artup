﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OpenAR : MonoBehaviour {

	public int isOn = 0;
	public Text ARButtonText;

	public GameObject LocationProvider;
	public GameObject DirectionalLight;
	public GameObject GMCameraSwitcher;
	public GameObject GMEventSystem;
	public GameObject GMMainCamera;
	public GameObject GMRecordingManager;
	public GameObject GPSHandler;

	public GameObject Map;
	public GameObject Player;
	public GameObject ViewNearby;
	public GameObject ListofArtworsk;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

	}

	public void OnMouseButton() {
		SceneManager.LoadScene ("AutomaticWorldSynchronization");
		/*
		if (isOn == 0) {
			isOn = 1;
			if (ARButtonText) ARButtonText.text = "AR Cam: ON";

			LocationProvider.SetActive (false);
			DirectionalLight.SetActive (false);
			GMCameraSwitcher.SetActive (false);
			GMEventSystem.SetActive (false);
			//GMMainCamera.SetActive (false);
			GMRecordingManager.SetActive (false);
			//GPSHandler.SetActive (false);

			Map.SetActive (false);
			Player.SetActive (false);
			ViewNearby.SetActive (false);
			ListofArtworsk.SetActive (false);

			SceneManager.LoadScene ("AutomaticWorldSynchronization", LoadSceneMode.Additive);
		} else {
			isOn = 0;
			if (ARButtonText) ARButtonText.text = "AR Cam: OFF";

			LocationProvider.SetActive (true);
			DirectionalLight.SetActive (true);
			GMCameraSwitcher.SetActive (true);
			GMEventSystem.SetActive (true);
			//GMMainCamera.SetActive (true);
			GMRecordingManager.SetActive (true);
			//GPSHandler.SetActive (true);

			Map.SetActive (true);
			Player.SetActive (true);
			ViewNearby.SetActive (true);
			ListofArtworsk.SetActive (true);

			SceneManager.UnloadSceneAsync ("AutomaticWorldSynchronization");
		}
		*/
	}
}
