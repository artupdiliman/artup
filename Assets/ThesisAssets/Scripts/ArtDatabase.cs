﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//using MapzenGo.Helpers;
//using MapzenGo.Models.Factories;
//using MapzenGo.Models.Plugins;
//using UniRx;
using GameSparks.Core;
using Mapbox.Utils;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Mapbox.Unity.Utilities
{
    public class ArtDatabase : MonoBehaviour
    {
        public ArtObject[] artworks;
        public List<ArtObject> nearbyArtworks = new List<ArtObject>();
		public Text nearbySculpture;
        public List<ArtObject> visibleArtworks = new List<ArtObject>();
        public GameObject[] artInstances;
        public double nearbyThreshold = 50;
        public double visibleThreshold = 50;

        public bool displayNearby = false;
        public bool displayAll = false;
		public bool displayComments = false;
		public bool displayReactions = false;

        private static ArtDatabase _instance = null;

        public static ArtDatabase instance
        {
            get { return _instance; }
        }

        public Text gpsLocationTextUI;

        public GUIStyle boxStyle = new GUIStyle();
        public GUIStyle labelStyle = new GUIStyle();

        // scrollable area for nearby artworks
        public GameObject scrollRect;
        public Transform scrollArea;

        // scrollable area for all artworks
        public GameObject scrollRectAll;
        public Transform scrollAreaAll;

		// scrollable area for comments
		public GameObject scrollRectComments;
		public Transform scrollAreaComments;

		// scrollable area for reactions
		public GameObject scrollRectReactions;
		public Transform scrollAreaReactions;

        public Transform artUIPrefab;
        public Transform artOnMapPrefab;

        public List<GameObject> artUIObjectsAll = new List<GameObject>();
		public List<GameObject> artUIObjectsComments = new List<GameObject> ();
		public List<GameObject> artUIObjectsReactions = new List<GameObject> ();

        public List<GameObject> artUIObjects = new List<GameObject>();
        public List<GameObject> artOnMapObjects = new List<GameObject>();

        public List<GameObject> artObjects = new List<GameObject>();

		public List<Transform> imageTargetObjects = new List<Transform>();
		public List<GameObject> ARObjects = new List<GameObject>();

		public bool nearbyUpdated = false;
        public bool visibleUpdated = false;
		public bool ARUpdated = false;
        protected float unityScale = 0.1635333f;

        public Sprite paintingSprite;
		public Sprite paintingARSprite;
        public Sprite sculptureSprite;
        public Sprite locationSprite;

		//for reactions
		public Sprite loveSprite;
		public Sprite happySprite;
		public Sprite sadSprite;
		public Sprite angerySprite;

        public Transform playerAvatar;
        public Transform exclamationPointIconPrefab;
        public Transform rescueBlobIconPrefab;
        public Transform cameraIconPrefab;

        public GameObject descriptionScreen;
        public Text descriptionUI;
        public Text artistUI;
        public Text titleUI;		
		public Image imageUI;
		public Sprite previewUnavailable;

		public Transform ARCameraPrefab;
		public GameObject ARCamera;

		public GameObject ARCameraButton;
		public GameObject nearbyArtButton;
		public GameObject GPSButton;

		public Camera NormalCameraObject;
		public Camera ARCameraObject;

		public Text ARCamButtonText;
		public Text GPSButtonText;
		public Text nearbyButtonText;

		private bool GPSinitialized = false;

		void Awake()
        {
            DontDestroyOnLoad(gameObject);

			//PlayerPrefs.DeleteAll();

			foreach (ArtObject art in artworks)
			{
				art.unlocked = (PlayerPrefs.GetInt("ArtID" + art.id.ToString() + "Unlocked", 0) == 0 ? false : true);
			}
        }


        void Start()
        {
            if (_instance)
            {
                Destroy(gameObject);
            }

            else
            {
                _instance = this;
            }

            nearbyArtworks.Clear();
            artUIObjects.Clear();
            artUIObjectsAll.Clear();
			artUIObjectsComments.Clear ();
			artUIObjectsReactions.Clear ();
			InitializeReactionsGUI ();
			UpdateComments ();
            InitializeAllArtworksGUI();
            // Initialize GUI Style
            //			boxStyle.alignment = TextAnchor.MiddleLeft;
        }

        void Update()
        {
            GetNearbyArtworks();
            if (gpsLocationTextUI) if (GPSHandler.instance) gpsLocationTextUI.text = "Latitude: " + Math.Round(GPSHandler.instance.latitude, 4) + "\nLongitude: " + Math.Round(GPSHandler.instance.longitude, 4);

			if (GPSinitialized == false) {
				if (GPSHandler.instance) {
					if (GPSHandler.instance.useGPS) {
						if (GPSButtonText) {
							GPSButtonText.text = "GPS  Mode:  On";
							if (GPSButton)
								GPSButton.GetComponent<Toggle> ().isOn = true;
							GPSHandler.instance.SwitchUseGPS (true);
						}	
					} else if (GPSButtonText) {
						GPSButtonText.text = "GPS  Mode:  Off";
						if (GPSButton)
							GPSButton.GetComponent<Toggle> ().isOn = false;
						GPSHandler.instance.SwitchUseGPS (false);
					}
					GPSinitialized = true;
				}
			}
		}

        void InitializeAllArtworksGUI()
        {
            foreach (ArtObject art in artworks)
            {
                if (art)
                {
                    art.nearby = false;
                    if (artUIPrefab)
                    {
                        // Transform artInstance = Instantiate(artUIPrefab, scrollAreaAll) as Transform;
                        Transform artInstance = Instantiate(artUIPrefab) as Transform;
                        artInstance.SetParent(scrollAreaAll, false);
                        artInstance.GetChild(0).GetComponent<Text>().text = art.name;
                        artInstance.GetChild(1).GetComponent<Text>().text = art.artist;
                        switch (art.type)
                        {
                            case ArtObject.artType.Painting:
								if (art.ARenabled) artInstance.GetChild(2).GetComponent<Image>().sprite = paintingARSprite;
								else artInstance.GetChild(2).GetComponent<Image>().sprite = paintingSprite;
								break;

                            case ArtObject.artType.Sculpture:
                                artInstance.GetChild(2).GetComponent<Image>().sprite = sculptureSprite;
                                break;

                            case ArtObject.artType.Location:
                                artInstance.GetChild(2).GetComponent<Image>().sprite = locationSprite;
                                break;

                            default:
                                break;
                        }
                        if (art.unlocked) artInstance.GetChild(2).GetComponent<Image>().color = new Color(0.25f, 0.25f, 0.25f, 1.0f);
                        else artInstance.GetChild(2).GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                        artUIObjectsAll.Add(artInstance.gameObject);
                    }
                }
            }
        }

		void InitializeReactionsGUI() {
			string[] reactions = { "Love", "Happy", "Sad", "Angry"};
			foreach (string reaction in reactions) {
				Transform artInstance = Instantiate (artUIPrefab) as Transform;
				artInstance.SetParent (scrollAreaReactions, false);
				artInstance.GetChild (0).GetComponent<Text> ().text = "0";
				switch (reaction)
				{
				case "Love":
					artInstance.GetChild(1).GetComponent<Text>().text = "Love";
					artInstance.GetChild(2).GetComponent<Image>().sprite = loveSprite;
					break;

				case "Angry":
					artInstance.GetChild(1).GetComponent<Text>().text = "Angry";
					artInstance.GetChild(2).GetComponent<Image>().sprite = angerySprite;
					break;

				case "Happy":
					artInstance.GetChild(1).GetComponent<Text>().text = "Happy";
					artInstance.GetChild(2).GetComponent<Image>().sprite = happySprite;
					break;

				case "Sad":
					artInstance.GetChild(1).GetComponent<Text>().text = "Sad";
					artInstance.GetChild(2).GetComponent<Image>().sprite = sadSprite;
					break;

				default:
					break;
				}
				artUIObjectsReactions.Add (artInstance.gameObject);
			}
		}

		public void UpdateComments() {
			foreach (GameObject artUI in artUIObjectsComments)
			{
				Destroy(artUI);
			}
				
			artUIObjectsComments.Clear ();

			new GameSparks.Api.Requests.LogEventRequest().SetEventKey("GET_COMMENTS")
				.SetEventAttribute ("COMMENT_ART_ID", GetNearbySculpture(nearbyArtworks))
				.Send((response) => {
				if (!response.HasErrors) {
					Debug.Log("Received Player Data From GameSparks...");
					List<GSData> locations = response.ScriptData.GetGSDataList ("comments");
					for (var e = locations.GetEnumerator (); e.MoveNext ();) {

						//string timestamp = e.Current.GetLong ("timestamp").ToString();
						string timestamp = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).ToLocalTime().AddMilliseconds(double.Parse(e.Current.GetLong ("timestamp").ToString())).ToString();
						double latitude = double.Parse(e.Current.GetString ("messLat"));
						double longitude = double.Parse(e.Current.GetString ("messLon"));
						string text = e.Current.GetString ("messText");
						string reacc = e.Current.GetString ("messReacc");

						if (artUIPrefab)
						{
							// Transform artInstance = Instantiate(artUIPrefab, scrollAreaAll) as Transform;
							Transform artInstance = Instantiate(artUIPrefab) as Transform;
							artInstance.SetParent(scrollAreaComments, false);
							artInstance.GetChild(0).GetComponent<Text>().text = text;
							switch (reacc)
							{
							case "love":
								artInstance.GetChild(1).GetComponent<Text>().text = timestamp;
								artInstance.GetChild(2).GetComponent<Image>().sprite = loveSprite;
								break;

							case "angery":
								artInstance.GetChild(1).GetComponent<Text>().text = timestamp;
								artInstance.GetChild(2).GetComponent<Image>().sprite = angerySprite;
								break;

							case "happy":
								artInstance.GetChild(1).GetComponent<Text>().text = timestamp;
								artInstance.GetChild(2).GetComponent<Image>().sprite = happySprite;
								break;

							case "sad":
								artInstance.GetChild(1).GetComponent<Text>().text = timestamp;
								artInstance.GetChild(2).GetComponent<Image>().sprite = sadSprite;
								break;

							default:
								break;
							}
							artUIObjectsComments.Add(artInstance.gameObject);
						}
					}
					int loveNumber = response.ScriptData.GetInt ("loveNumber").GetValueOrDefault(-1);
					int happyNumber = response.ScriptData.GetInt ("happyNumber").GetValueOrDefault(-1);
					int sadNumber = response.ScriptData.GetInt ("sadNumber").GetValueOrDefault(-1);
					int angeryNumber = response.ScriptData.GetInt ("angeryNumber").GetValueOrDefault(-1);
					foreach (GameObject reactUI in artUIObjectsReactions) {
							switch (reactUI.transform.GetChild(1).GetComponent<Text>().text)
							{
							case "Love":
									reactUI.transform.GetChild(0).GetComponent<Text>().text = loveNumber.ToString();
								break;

							case "Angry":
									reactUI.transform.GetChild(0).GetComponent<Text>().text = angeryNumber.ToString();
								break;

							case "Happy":
									reactUI.transform.GetChild(0).GetComponent<Text>().text = happyNumber.ToString();
								break;

							case "Sad":
									reactUI.transform.GetChild(0).GetComponent<Text>().text = sadNumber.ToString();
								break;

							default:
								break;
							}
					}
				} else {
					Debug.Log("Error Loading Message Data...");
				}
			});
		}

        void GetNearbyArtworks()
        {
            //			if (Input.location.status != LocationServiceStatus.Failed)
            //			{
            //				Vector2d currLocation = GM.LatLonToMeters(Input.location.lastData.latitude, Input.location.lastData.longitude);

            if (GPSHandler.instance)
            {
                //				Vector2d currLocation = GM.LatLonToMeters(GPSHandler.instance.latitude, GPSHandler.instance.longitude);
                //				Debug.Log("this works");

                foreach (ArtObject art in artworks)
                {
                    if (art)
                    {
                        //Vector2d artLocation = GM.LatLonToMeters(art.location.latitude, art.location.longitude);

                        Vector2d locationDiffLatLon = new Vector2d(GPSHandler.instance.latitude - art.location.latitude, GPSHandler.instance.longitude - art.location.longitude);
						Vector2d locationDiffMeters = Conversions.LatLonToMeters(locationDiffLatLon.x, locationDiffLatLon.y);
						//Vector2d locationDiffMeters = Conversions.LatLonToMeters(locationDiffLatLon);
                        //Debug.Log(currLocation.x + " " + currLocation.y + "  ,  " + artLocation.x + " " + artLocation.y + "  ,  " + Vector2d.Distance(currLocation, artLocation));

                        if ((Vector2d.Distance(locationDiffMeters, Vector2d.zero) < nearbyThreshold) || (Vector2d.Distance(locationDiffMeters, Vector2d.zero) < visibleThreshold))
                        {
                            if (Vector2d.Distance(locationDiffMeters, Vector2d.zero) < nearbyThreshold)
                            {
                                if (!nearbyArtworks.Contains(art))
                                {
                                    nearbyArtworks.Add(art);

                                    if (art.type == ArtObject.artType.Painting && art.ARenabled == true)
                                    {
                                        if (art.imageTarget) // Instantiate(art.imageTarget);
																				{
																					// Transform imgTrgt = Instantiate(art.imageTarget) as Transform;
																					//imageTargetObjects.Add(art.imageTarget);
																					ARUpdated = true;
																					// imageTargetObjects.Add(imgTrgt.gameObject);

																				}
                                    }

                                    nearbyUpdated = true;
                                }

                                if (!visibleArtworks.Contains(art))
                                {
                                    visibleArtworks.Add(art);
                                    visibleUpdated = true;
                                }

                                art.nearby = true;
                                art.distance = Vector2d.Distance(locationDiffMeters, Vector2d.zero);
                            }
                            else if (Vector2d.Distance(locationDiffMeters, Vector2d.zero) < visibleThreshold)
                            {
                                if (nearbyArtworks.Contains(art))
                                {
                                    nearbyArtworks.Remove(art);
                                    nearbyUpdated = true;
                                    art.nearby = false;

									                  if (art.type == ArtObject.artType.Painting && art.ARenabled == true)
                                    {
                                        if (art.imageTarget)
																				{
																					//imageTargetObjects.Remove(art.imageTarget);
																					ARUpdated = true;
																				}
                                    }
                                }

                                if (!visibleArtworks.Contains(art))
                                {
                                    visibleArtworks.Add(art);
                                    visibleUpdated = true;
                                }
                                // art.nearby = true;
                                art.distance = Vector2d.Distance(locationDiffMeters, Vector2d.zero);
                            }
                        }
                        else
                        {
                            if (nearbyArtworks.Contains(art))
                            {
                                nearbyArtworks.Remove(art);
                                nearbyUpdated = true;
                            }

                            if (visibleArtworks.Contains(art))
                            {
                                visibleArtworks.Remove(art);
                                visibleUpdated = true;
                            }

														if (art.type == ArtObject.artType.Painting && art.ARenabled == true)
                            {
                                if (art.imageTarget)
																{
																	//imageTargetObjects.Remove(art.imageTarget);
																	ARUpdated = true;
																}
                            }

                            art.nearby = false;
                            art.distance = 0;
                        }
                    }
                }
            }

						if (ARUpdated)
						{
							ARUpdated = false;
							//UpdateImageTargets();
						}

            if (nearbyUpdated) // && displayNearby)
            {
                nearbyUpdated = false;
                visibleUpdated = false;
                UpdateNearbyUI();
                UpdateVisibleUI();
				UpdateComments ();
            }

            else if (visibleUpdated)
            {
                visibleUpdated = false;
                UpdateVisibleUI();
            }
        }

        public void ShowAllArtworks()
        {
            displayAll = !displayAll;

            if (displayAll)
            {
                scrollRectAll.SetActive(true);
            }
            else
            {
                scrollRectAll.SetActive(false);
            }

        }

        public void ShowNearby()
        {
            displayNearby = !displayNearby;
			//if (!SFXManager.instance.GetComponent<AudioSource>().isPlaying && nearbyArtButton.GetComponent<Toggle>().IsInteractable())	SFXManager.instance.PlaySound (0);
            if (displayNearby)
            {
                scrollRect.SetActive(true);
								nearbyButtonText.text = "Hide  Nearby";

            }
            else
            {
                scrollRect.SetActive(false);
								nearbyButtonText.text = "Show  Nearby";
            }
				
        }

		public void ShowComments() {
			displayComments = !displayComments;

			if (displayComments)
			{
				scrollRectComments.SetActive(true);
			}
			else
			{
				scrollRectComments.SetActive(false);
			}
		}

		public void ShowReactions() {
			displayReactions = !displayReactions;

			if (displayReactions)
			{
				scrollRectReactions.SetActive(true);
			}
			else
			{
				scrollRectReactions.SetActive(false);
			}
		}

			public void HideNearby()
			{
					displayNearby = false;
					scrollRect.SetActive(false);
					nearbyButtonText.text = "Show  Nearby";
			}

		/*
				public void CreateNewARCamera()
				{
					// Destroy Old Camera
					Destroy(ARCamera);

					// Create New Camera from Prefab
					Transform newCam = Instantiate(ARCameraPrefab) as Transform;
					ARCamera = newCam.gameObject;
					ARCameraObject  = newCam.GetChild(0).GetComponent<Camera>();
				}
		*/
				public void UpdateImageTargets()
				{
						DeactivateARCamera();
						// ARCamera.SetActive(false);

						//CreateNewARCamera();
						foreach (GameObject a in ARObjects)
            {
                Destroy(a);
            }

						ARObjects.Clear();

						foreach (Transform imgTrgt in imageTargetObjects)
						{
							Transform t = Instantiate(imgTrgt) as Transform;
							ARObjects.Add(t.gameObject);
						}
					}

        public void UpdateNearbyUI()
        {
            foreach (GameObject artUI in artUIObjects)
            {
                Destroy(artUI);
            }

            artUIObjects.Clear();
						bool showARButton = false;
					
            foreach (ArtObject art in nearbyArtworks)
            {
                if (art)
                {
                    if (artUIPrefab)
                    {
                        Transform artInstance = Instantiate(artUIPrefab) as Transform;
                        artInstance.SetParent(scrollArea, false);
                        artInstance.GetChild(0).GetComponent<Text>().text = art.name;
                        artInstance.GetChild(1).GetComponent<Text>().text = art.artist;
												artInstance.GetComponent<ArtObjectInfo>().SetId(art.id); 
                        switch (art.type)
                        {
                            case ArtObject.artType.Painting:
																if (art.ARenabled)
																{
																	artInstance.GetChild(2).GetComponent<Image>().sprite = paintingARSprite;
																	showARButton = true;
																}
																else artInstance.GetChild(2).GetComponent<Image>().sprite = paintingSprite;
																break;

							case ArtObject.artType.Sculpture:
								artInstance.GetChild (2).GetComponent<Image> ().sprite = sculptureSprite;
								showARButton = true;
                                break;

                            case ArtObject.artType.Location:
                                artInstance.GetChild(2).GetComponent<Image>().sprite = locationSprite;
                                break;

                            default:
                                break;
                        }
                        if (art.unlocked) artInstance.GetChild(2).GetComponent<Image>().color = new Color(0.25f, 0.25f, 0.25f, 1.0f);
                        else artInstance.GetChild(2).GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

                        artUIObjects.Add(artInstance.gameObject);
                    }
                }
            }

					if (nearbyArtworks.Count == 0)
					{
						if (nearbyArtButton) {
						//nearbyArtButton.GetComponent<Toggle>().interactable = false;
					 	//nearbyArtButton.GetComponent<Toggle>().isOn = false; 
						}// nearbyArtButton.SetActive(false);
						HideNearby();
						nearbyButtonText.text = "None  Nearby";
						if (ARCameraButton)
						{
							// ARCameraButton.SetActive(false);
							//ARCameraButton.GetComponent<Toggle>().interactable = false;
							//if(ARCameraObject.enabled) SwitchCam();
						}
					}
					else
					{
						if (SFXManager.instance)
							if (!SFXManager.instance.GetComponent<AudioSource>().isPlaying) SFXManager.instance.PlaySound (1);
						if (displayNearby) HideNearby();
						if (nearbyArtButton) { nearbyArtButton.GetComponent<Toggle>().interactable = true; nearbyArtButton.GetComponent<Toggle>().isOn = true; HideNearby(); }// nearbyArtButton.SetActive(true);
						if (showARButton)
						{if (ARCameraButton) ARCameraButton.GetComponent<Toggle>().interactable = true;}// ARCameraButton.SetActive(true);}
					else
						{
							if (ARCameraButton)
							{
								// ARCameraButton.SetActive(false);
								//ARCameraButton.GetComponent<Toggle>().interactable = false;
								//if(ARCameraObject.enabled) SwitchCam();
							}
						}
					}


				}

        public void UpdateVisibleUI()
        {

            foreach (GameObject artOnMap in artOnMapObjects)
            {
                Destroy(artOnMap);
            }
            artOnMapObjects.Clear();

            foreach (ArtObject art in visibleArtworks)
            {
                if (art)
                {
                    if (artOnMapPrefab)
                    {
						//Vector2d v3 = Conversions.LatLonToMeters(new Vector2d(TileManager.instance.InitialCenter.y - art.location.latitude, TileManager.instance.InitialCenter.x - art.location.longitude));
						Vector2d v3 = Conversions.LatLonToMeters(new Vector2d(14.648629 - art.location.latitude, 121.068567 - art.location.longitude));
                        //Vector3 artPosOnMap = new Vector3(((float)v3.x) * TileManager.instance.unityScale * -1, 4.0f, ((float)v3.y) * TileManager.instance.unityScale * -1 /*+ (playerAvatar.position.z / 40.0f)*/);
						Vector3 artPosOnMap = new Vector3(((float)v3.x) * 15 * -1, 4.0f, ((float)v3.y) * 15 * -1 /*+ (playerAvatar.position.z / 40.0f)*/);
                        Transform artOnMapInstance = Instantiate(artOnMapPrefab, artPosOnMap, Quaternion.Euler(90, 0, 0)) as Transform;
                        switch (art.type)
                        {
                            case ArtObject.artType.Painting:
																if (art.ARenabled) artOnMapInstance.GetComponent<SpriteRenderer>().sprite = paintingARSprite;
																else artOnMapInstance.GetComponent<SpriteRenderer>().sprite = paintingSprite;
                                break;

                            case ArtObject.artType.Sculpture:
                                artOnMapInstance.GetComponent<SpriteRenderer>().sprite = sculptureSprite;
                                break;

                            case ArtObject.artType.Location:
                                artOnMapInstance.GetComponent<SpriteRenderer>().sprite = locationSprite;
                                break;

                            default:
                                break;
                        }
                        artOnMapInstance.GetComponent<ArtObjectOnMap>().id = art.id;
                        artOnMapObjects.Add(artOnMapInstance.gameObject);

                        if (art.unlocked) artOnMapInstance.GetComponent<SpriteRenderer>().color = new Color(0.25f, 0.25f, 0.25f, 1.0f);
                        else artOnMapInstance.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

                        if (art.nearby)
                        {
                            if (!art.unlocked)
                            {
                                if (rescueBlobIconPrefab)
                                {
                                    Transform rescIcon = Instantiate(rescueBlobIconPrefab) as Transform;
                                    rescIcon.SetParent(artOnMapInstance);
                                    rescIcon.localPosition = new Vector3(-8.0f, 8.0f, 0);
                                    artOnMapObjects.Add(rescIcon.gameObject);
                                }
                            }

                            if (art.ARenabled)
                            {
                                if (cameraIconPrefab)
                                {
                                    Transform camIcon = Instantiate(cameraIconPrefab) as Transform;
                                    camIcon.SetParent(artOnMapInstance);
                                    camIcon.localPosition = new Vector3(8.0f, 8.0f, 0);
                                    artOnMapObjects.Add(camIcon.gameObject);
                                }
                            }
                            else
                            {
                                if (exclamationPointIconPrefab)
                                {
                                    Transform excIcon = Instantiate(exclamationPointIconPrefab) as Transform;
                                    excIcon.SetParent(artOnMapInstance);
                                    excIcon.localPosition = new Vector3(8.0f, 8.0f, 0);
                                    artOnMapObjects.Add(excIcon.gameObject);
                                }
                            }
                        }
                    }
                }
            }

        }

        public ArtObject GetArtById(int id)
        {
            foreach (ArtObject art in artworks)
            {
                if (art)
                {
                    if (art.id == id)
                    {
                        return art;
                    }
                }
            }

            return null;
        }

        public void UnlockArt(int artId)
        {
            ArtObject art = GetArtById(artId);
            if (art)
            {
				if (!art.unlocked) 
				{
					if (SFXManager.instance)
						SFXManager.instance.PlayOneShot (2);
					art.unlocked = true;
					if (PlayerData.instance) {
						PlayerData.instance.LevelUp ();
						Debug.Log (PlayerPrefs.GetInt ("PlayerLevel", -1));
						Debug.Log ("hewwo");
					}
					//if (PlayerData.instance) PlayerData.instance.LevelUp ();
					Debug.Log ("I unlocked something.");
					PlayerPrefs.SetInt ("ArtID" + art.id.ToString () + "Unlocked", 1);
					UpdateNearbyUI ();
					UpdateVisibleUI ();
					artUIObjectsAll[artId - 1].GetComponent<Transform>().GetChild(2).GetComponent<Image>().color = new Color(0.25f, 0.25f, 0.25f, 1.0f);
				}
            }
        }

		public bool CheckUnlocked(int artId)
		{
			ArtObject art = GetArtById(artId);
			if (art)
			{
				return art.unlocked;
			}
			return false;
		}

        /*
		public void DisplayInformation(int artId)
        {
            ArtObject art = GetArtById(artId);

            if (art)
            {
				if (!art.ARenabled) {
					if (art.nearby) {
						if (!art.unlocked) {
							UnlockArt (art.id);
							UpdateNearbyUI ();
							UpdateVisibleUI ();
						}

						if (art.image)
							imageUI.sprite = art.image;
						else
							imageUI.sprite = previewUnavailable;
						descriptionUI.text = art.description;
						titleUI.text = art.name;
						artistUI.text = art.artist;

						descriptionScreen.SetActive (true);
					}
				} else 
				{
					if (art.nearby)
					{
						if (art.image)
							imageUI.sprite = art.image;
						else
							imageUI.sprite = previewUnavailable;
						descriptionUI.text = "Switch on the AR Camera and point it at the artwork to view its description!";
						titleUI.text = art.name;
						artistUI.text = art.artist;

						descriptionScreen.SetActive(true);
					}
				}
					
            }
        }
        */

		public void DisplayInformation(int artId)
		{
			ArtObject art = GetArtById(artId);

			if (art)
			{
					if (art.nearby) {
						if (!art.unlocked) {
							UnlockArt (art.id);
							UpdateNearbyUI ();
							UpdateVisibleUI ();
						}

						if (art.image)
							imageUI.sprite = art.image;
						else
							imageUI.sprite = previewUnavailable;
						descriptionUI.text = art.description;
						titleUI.text = art.name;
						artistUI.text = art.artist;

						descriptionScreen.SetActive (true);
					}
			}
		}

        public void HideInformation()
        {
            descriptionScreen.SetActive(false);
        }

			public void ActivateARCamera()
			{
				ARCameraButton.GetComponent<Toggle>().isOn = true;
				ARCamera.SetActive(true);

				foreach (GameObject i in ARObjects)
				{
					i.SetActive(true);
				}
/*				foreach (Transform i in imageTargetObjects)
				{
					Transform imgTrgt = Instantiate(i) as Transform;
					ARObjects.Add(imgTrgt);
				}*/

			}

			public void DeactivateARCamera()
			{
				ARCameraButton.GetComponent<Toggle>().isOn = false;
				foreach (GameObject i in ARObjects)
				{
					i.SetActive(false);
					// Destroy(i.gameObject);
				}

				// ARObjects.Clear();
				ARCamera.SetActive(false);

				if (RecordingManager.instance)
					RecordingManager.instance.GetComponent<AudioSource> ().Stop ();
			}

			public void SwitchCam()
			{
				if (NormalCameraObject.enabled)
				{
					ActivateARCamera();
					NormalCameraObject.enabled = false;
					ARCameraObject.enabled = true;
					if (ARCamButtonText) ARCamButtonText.text = "AR Cam:  On";
				}
				else
				{
					DeactivateARCamera();
					NormalCameraObject.enabled = true;
					ARCameraObject.enabled = false;
					if (ARCamButtonText) ARCamButtonText.text = "AR Cam:  Off";
				}
			}

		public void SwitchGPS()
		{
			if (GPSHandler.instance) GPSHandler.instance.SwitchUseGPS();
			if (GPSHandler.instance.useGPS)
			{
				if (GPSButtonText) GPSButtonText.text = ("GPS  Mode:  On");
				
			}
			else if (GPSButtonText) GPSButtonText.text = ("GPS  Mode:  Off");
		}

		public void PlayRecordingByID (int id)
		{
			if (RecordingManager.instance)
				RecordingManager.instance.PlayRecording (GetArtById (id).recording);
		}

		public int GetNearbySculpture (List<ArtObject> nearby) {
			foreach (ArtObject i in nearby) {
				if (i.type == ArtObject.artType.Sculpture) {
					nearbySculpture.text = i.id.ToString ();
					return i.id;
				}
			}
			return -1;
		}
    }
}