﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mapbox.Unity.Utilities
{
    public class ArtUnlocker : MonoBehaviour
    {
        public int id = 0;

		void Start()
		{
			if (ArtDatabase.instance) if (ArtDatabase.instance.CheckUnlocked(id)) GetComponent<SpriteRenderer>().color = new Color(0.25f, 0.25f, 0.25f, 1.0f);
		}

        void OnMouseDown()
        {
			if (ArtDatabase.instance) ArtDatabase.instance.UnlockArt(id);
        }
    }
}
