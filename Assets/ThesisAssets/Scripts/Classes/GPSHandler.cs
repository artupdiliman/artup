﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GPSHandler : MonoBehaviour 
{

	private GUIStyle testStyle  = new GUIStyle();

	private static GPSHandler _instance = null;

	public static GPSHandler instance 
	{
		get { return _instance; }
	}

	public double latitude = 0;
	public double longitude = 0;
	public float secsWait = 0;

	public bool loop = true;

	public void Awake()
	{
		DontDestroyOnLoad(this);
	}

	public bool useGPS = true;

	public GameObject intro;

	IEnumerator Start()
	{
		if (_instance) 
		{
			Destroy(gameObject);
		}

		else 
		{
			_instance = this;
		}

		#if UNITY_EDITOR || UNITY_STANDALONE
		yield return new WaitForSeconds(1);
		useGPS = true;
		if (intro) intro.SetActive(true);
		#else
		Debug.Log("I started.");
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser)
			yield break;

		// Start service before querying location
		Input.location.Start();

		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return new WaitForSeconds(1);
			maxWait--;
		}

		// Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			print("Timed out");
			if (intro) intro.SetActive(true);
			useGPS = false;
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			print("Unable to determine device location");
			useGPS = false;
			if (intro) intro.SetActive(true);
			// Application.LoadLevel(1);
			yield break;

		}
		else
		{
			latitude = Input.location.lastData.latitude;
			longitude = Input.location.lastData.longitude;
			if (intro) intro.SetActive(true);
			// Application.LoadLevel(1);
			// Access granted and location value could be retrieved
			print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
		}

		// Stop service if there is no need to query location updates continuously
		// Input.location.Stop();

        /*
		while (true)
		{
		yield return StartCoroutine("GetGPSLocation");
		yield return new WaitForSeconds(secsWait);
		}
        */
		if (intro) intro.SetActive(true);
    	// Application.LoadLevel(1);
#endif
	}

	IEnumerator GetGPSLocation() 
	{
		
		if (!Input.location.isEnabledByUser)
			yield break;

		// Start service before querying location
		Input.location.Start();

		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return new WaitForSeconds(1);
			maxWait--;
		}

		// Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			print("Timed out");
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			print("Unable to determine device location");
			yield break;
		}
		else
		{
			latitude = Input.location.lastData.latitude;
			longitude = Input.location.lastData.longitude;
		}

		// Stop service if there is no need to query location updates continuously
		Input.location.Stop();

		yield break;
	}


	void Update()
	{
		if (useGPS)
		{
			#if UNITY_EDITOR || UNITY_STANDALONE
						if (Input.GetKey(KeyCode.W)) latitude += 0.0001;
							else if (Input.GetKey(KeyCode.S)) latitude -= 0.0001;

							if (Input.GetKey(KeyCode.A)) longitude -= 0.0001;
							else if (Input.GetKey(KeyCode.D)) longitude += 0.0001;

			#else
					if (secsWait >= 10)
					{
						if (Input.location.status != LocationServiceStatus.Failed)
						{
							latitude = Input.location.lastData.latitude;
							longitude = Input.location.lastData.longitude;
						}

						secsWait = 0;
					}
					else
					{
						secsWait += Time.deltaTime;
					}

			#endif
		}
		else
		{
			// latitude = 14.6549;
			// longitude = 121.070841;
		}
  }

	public void SwitchUseGPS()
	{
		useGPS = !useGPS;
	}

	public void SwitchUseGPS(bool status)
	{
		useGPS = status;
	}

	public void StartGame()
	{
		SceneManager.LoadScene ("LocationProvider");
	}
}
