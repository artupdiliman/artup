﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ArtObject : MonoBehaviour 
{
    // remove this later, for testing purposes only

    public int id;
	public string name;
	public string artist;

	[System.Serializable]
	public class gpsLocation
	{
		public double latitude;
		public double longitude;
	}

	public enum artType
	{
		Painting,
		Sculpture,
		Location
	}

	public enum distType
	{
		Radius,
		BoundingBox
	}

	public gpsLocation location;
	public artType type;

	public Sprite image;
	[TextArea]
	public string description;
	
	private bool displayDescription = false;
	private GUIStyle testStyle  = new GUIStyle();
	
	public bool nearby = false;
	public bool within = false;

	public double distance = 0;

	public float radius = 100;

	public bool ARenabled = false;
	public Transform imageTarget;
	public AudioClip recording;

    public bool unlocked = false;
}
	