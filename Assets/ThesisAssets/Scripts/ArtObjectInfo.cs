﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mapbox.Unity.Utilities
{
	public class ArtObjectInfo : MonoBehaviour
	{
		public int id = 0;

		public void SetId(int newId)
		{
			id = newId;
		}

		public void DisplayInfo()
		{
			if (ArtDatabase.instance) ArtDatabase.instance.DisplayInformation(id);
			if (SFXManager.instance)
				SFXManager.instance.PlayOneShot (0);
		}
	}
}