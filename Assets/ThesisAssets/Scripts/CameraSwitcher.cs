﻿using UnityEngine;
using System.Collections;

namespace Mapbox.Unity.Utilities
{
	public class CameraSwitcher : MonoBehaviour
	{
		public Camera NormalCamera;
		public Camera ARCamera;
		public GameObject NormalCameraObject;
		public GameObject ARCameraObject;

		// Instantiate ON Camera Switch

		public void SwitchCam()
		{
			if (NormalCamera.enabled)
			{
				ArtDatabase.instance.ActivateARCamera();
				NormalCamera.enabled = false;
				ARCamera.enabled = true;
			}
			else
			{
				ArtDatabase.instance.DeactivateARCamera();
				NormalCamera.enabled = true;
				ARCamera.enabled = false;
			}
		}
	}
}