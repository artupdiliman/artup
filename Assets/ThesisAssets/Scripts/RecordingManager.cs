﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordingManager : MonoBehaviour {

	private AudioSource src;

	private static RecordingManager _instance = null;

	public static RecordingManager instance
	{
		get { return _instance; }
	}


	public void Start()
	{
		if (_instance)
		{
			Destroy(gameObject);
		}

		else
		{
			_instance = this;
		}

		src = GetComponent<AudioSource> ();
	}

	public void PlayRecording (AudioClip clip)
	{
		if (clip) {
			src.clip = clip;
			src.Play ();
		}
	}

	public void StopRecording ()
	{
		if (src.isPlaying) src.Stop ();
	}

}
