﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Float : MonoBehaviour 
{
	public Vector3 origPos;

	public float amplitude = 0.5f;
	public float frequency = 1;

	// Use this for initialization
	void Start () 
	{
		origPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.localPosition = new Vector3(origPos.x, origPos.y, origPos.z + Mathf.Sin(2.0f * Mathf.PI * amplitude * frequency * Time.time));
	}
}
