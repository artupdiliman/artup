﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mapbox.Unity.Utilities
{
	public class RecordingButton : MonoBehaviour {

		public int id = 0;

		void OnMouseDown()
		{
			if (SFXManager.instance)
				SFXManager.instance.PlaySound (0);
			if (ArtDatabase.instance) ArtDatabase.instance.PlayRecordingByID(id);
		}
	}
}