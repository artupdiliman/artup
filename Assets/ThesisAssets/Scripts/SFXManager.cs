﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour 
{
	private AudioSource src;
	public AudioClip[] SFX;

	private static SFXManager _instance = null;

	public static SFXManager instance
	{
		get { return _instance; }
	}


	public void Start()
	{
		if (_instance)
		{
			Destroy(gameObject);
		}

		else
		{
			_instance = this;
		}

		src = GetComponent<AudioSource> ();
	}

	public void PlaySound (string name)
	{
		switch (name)
		{
			default:
			break;

			case "button":
			PlaySound (0);
			break;
	
			case "unlock":
			PlaySound (2);
			break;

			case "notification":
			PlaySound (1);
			break;
		}
	}

	public void PlaySound (int id)
	{
		src.clip = SFX [id];
		src.Play ();
	}

	public void PlayOneShot (int id)
	{
		src.PlayOneShot (SFX [id]);
	}
}
