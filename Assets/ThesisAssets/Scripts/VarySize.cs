﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VarySize : MonoBehaviour 
{
	public Vector3 origSize;

	public float amplitude = 0.5f;
	public float frequency = 1;
	public float amountVariation = 0.1f;

	// Use this for initialization
	void Start () 
	{
		origSize = transform.localScale;
	}

	// Update is called once per frame
	void Update () 
	{
		transform.localScale = new Vector3(origSize.x  + Mathf.Sin(2.0f * Mathf.PI * amplitude * frequency * Time.time) * origSize.x * amountVariation, origSize.y - Mathf.Sin(2.0f * Mathf.PI * amplitude * frequency * Time.time) * origSize.y * amountVariation, origSize.z);
	}
}
