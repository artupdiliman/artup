﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour 
{

	private static PlayerData _instance = null;

	public static PlayerData instance
	{
		get { return _instance; }
	}

	public int level = 0;
  public int blobs = 0;

	public Sprite currSprite;
	public Sprite[] levelIcons;
	public Image playerIcon;
	//public SpriteRenderer playerAvatar;

	public Text levelUI;

	public void Awake()
	{
		level = PlayerPrefs.GetInt("PlayerLevel", 0);
		if (levelUI) levelUI.text = level.ToString();

		UpdateSprite();
	}

	void Start()
	{
		if (_instance)
		{
			Destroy(gameObject);
		}

		else
		{
			_instance = this;
		}
	}

	public void Update()
	{
		if (levelUI) levelUI.text = ("Lv. " + level.ToString());
	}

	public void LevelUp ()
	{
		level++;
		PlayerPrefs.SetInt("PlayerLevel", level);
		UpdateSprite ();
	}

	public void UpdateSprite()
	{
		for (int x = 0; x <= level; x++)
		{
			if (levelIcons[x])
			{
				currSprite = levelIcons[x];
			}
		}

		playerIcon.sprite = currSprite;
		//playerAvatar.sprite = currSprite;
	}
}